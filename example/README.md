# This directory contains all the examples

## Run examples

```
cmake -S . -B build -DBUILD_EXAMPLES=ON
cd build && make -j
./example/register-example
```
