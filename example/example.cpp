#include <yaml-cpp/yaml.h>

#include <../ext/string_id/string_id.hpp>
#include <fstream>
#include <iostream>
#include <linux_register.hpp>
#include <map>
#include <set>
#include <vector>

/* Use string_id literals */
using namespace foonathan::string_id::literals;

/* Use Linux::Register */
using namespace Linux;

/* Comparator for std::set for Register */
struct cmp {
  bool operator()(const Register& a, const Register& b) const {
    return a.address != b.address && a.bitmask != b.bitmask;
  }
};

static const std::map<foonathan::string_id::hash_type, RegisterInfo> info{
    {"abc"_id, {"abc", 0xFA}}, {"a"_id, {"a", 0xFF}}};

/* Demonstrate how to push different type of (std::uint8_t and std::uint16_t)
 * registers to a single container. Demonstrate how to read register infos from
 * yaml file. */
int main() {
  /* Parse register info from yaml file */
  // TODO: parsing expects data is always valid
  const auto file = "register_info.yaml";
  try {
    const auto& yaml = YAML::LoadFile(file);
    const auto& registers = yaml["organisations"][0]["registers"];
    if (registers.IsSequence()) {
      std::cout << "Parsed registers: \n";
      for (const auto& r : registers) {
        const auto ri = r.as<RegisterInfo>();
        std::cout << " - \"" << std::get<const char*>(ri.name) << "\"\n";
      }
    }
  } catch (YAML::Exception& e) {
    std::cerr << e.what() << "\n";
    return EXIT_FAILURE;
  }

  /* Strings to hashes in compile-time */
  std::cout << "\"string1\"_id: " << (unsigned)"string1"_id << '\n';
  std::cout << "\"string2\"_id: " << (unsigned)"string2"_id << '\n';

  /* Simplest use case for Register */
//  Register r1{{.name = "rx"_id, .address = 0xFF}, std::uint16_t(0xF0FF)};
//  std::cout << r1 << '\n';
  /* Same as above but with "rx"_id just "rx" */
  Register r1{{.name = "rx", .address = 0xFF}, std::uint16_t(0xF0FF)};
  std::cout << r1 << '\n';

  /* Value calculated compile-time */
  constexpr Register r2({.name = "ry", .address = 0xCD, .bitmask = 0x1},
                        std::uint8_t(0xFF));
  std::cout << r2 << '\n';
  constexpr auto v2 = get_val_from(r2);
  static_assert(v2 == 1, "value was not 1");

  constexpr Register r3{{.name = "rz", .address = 0xCD, .bitmask = 0xF},
                        std::uint16_t(0xFFFF)};
  std::cout << r3 << '\n';
  constexpr auto v3 = get_val_from(r3);
  static_assert(v3 == 15, "value was not 15");

  constexpr Register r4{{.name = "rx", .address = 0xFF}, std::uint16_t(12)};
  static_assert(r4.has_value() == true, "should be true");
  constexpr auto v4 = get_val_from(r4);

  constexpr Register r5{{.name = "rx", .address = 0xFF}};
  static_assert(r5.has_value() == false, "should be false");
  // This fails to compile:
  // constexpr auto v5 = get_val_from(r5);

  /* Show std::vector */
  std::vector<Register> regs{r1, r2, r3};
  std::cout << "registers in a vector: " << regs.size() << '\n';

  /* Show std::set with custom comparator */
  std::set<Register, cmp> sregs;
  sregs.insert(r1);
  sregs.insert(r2);
  sregs.insert(r3);
  std::cout << "registers in a set: " << sregs.size() << '\n';

  auto ri1 = info.at("abc"_id);
  auto reg1 = Register{RegisterInfo{info.at("abc"_id)}, static_cast<std::uint8_t>(0)};
  auto reg2 = Register{RegisterInfo{info.at("a"_id)}, static_cast<std::uint8_t>(1)};
  auto reg3 = Register{std::move(ri1), static_cast<std::uint8_t>(2)};
  std::cout << reg1 << ", " << reg2 << ", " << reg3 << '\n';
}