# External libraries used by this library

##  [string_id](https://github.com/foonathan/string_id)

Store strings as 64 bit numbers (hashes) during compile-time

### Compile switches

Quote from `string_id` repo:

* **FOONATHAN_STRING_ID_DATABASE** - if OFF, the database is disabled completely, e.g. the dummy database is used. This does not allow retrieving strings or collision checking but does not need that much memory. It is ON by default.

* **FOONATHAN_STRING_ID_MULTITHREADED** - if ON, database access will be synchronized via a mutex, e.g. the thread safe adapter will be used. It has no effect if database is disabled. Default value is ON.

## [googletest](https://github.com/google/googletest)

Tests of [test](https://gitlab.com/osteri/register/blob/main/test/) directory