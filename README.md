# Generic register library

Almost all registers have similar kind of behaviour; they usually have an address, value and possibly a name. This library tries to abstract this behaviour away from implementations. If the value isn't fetched, try to fetch it again. If the value isn't received a certain window, the register is to be considered "timed out". The value can have certain type and masked bits. This library currently support only numeric registers (so for example ```std::string``` isn't supported).

```Portable::Register``` is a generic base class meant for user defined implementions. User defined implementation should derive from ```Portable::Register``` and implement own Register based on the platform the Register is being implemented on. With this approach, user can optimize register class for different platforms. For example: in embedded platform user might want to save memory by leaving out ```<iostream>``` and operators for printing ```Register``` in to ```std::ostream```. In Linux we can implement a derived class in Linux namespace which makes printing in ```std::cout``` possible as the example shows.

## Linux

Superset implementation of `Portable::Register`. This should be the default selection for any platform where memory isn't the biggest concern.

```cpp
#include <vector>
#include "linux_register.hpp"

using namespace Linux;

int main() {
  Register r1({.name = "register-X", .address = 0xCD, .mask_bits = 0x1},
              (std::uint8_t)0xFF); // std::uint8_t
  std::cout << r1         // Linux has implemented stream operator for std::ostream
            << '\n';

  Register r2({.name = "register-X", .address = 0xCD},
              (std::uint16_t)0x0FFF);  // std::uint16_t
  std::vector v{r1, r2};  // Push different types of registers (uint16_t and
                          // uint8_t) in to the same container, this is possible
                          // because we use std::variant under the hood
  std::cout << r2.get<std::uint16_t>() << '\n';
}
```

## RTOS

Minimal implementation of `Portable::Register`. Doesn't support `std::cout` and other functionality which wouldn't make sense in embedded. The aim of this implementation is to have low memory footprint. 

```cpp
#include "rtos_register.hpp"

using namespace RTOS;

int main() {
  Register r1({.name = "register-X"_id, .address = 0xCD, .mask_bits = 0x1},
              (std::uint8_t)0xFF); // std::uint8_t
}
```

See more usage examples in [test](https://gitlab.com/osteri/register/blob/main/test/) and [example](https://gitlab.com/osteri/register/blob/main/example/) directories.

## Adding library as a git submodule in CMake

First clone the library:
```
git submodule add https://gitlab.com/osteri/register
```
Then include this library in your main `CMakeLists.txt` like this:
```
add_subdirectory(register)
target_link_libraries(main LINK_PUBLIC register-lib)
```

## Build all

```
cmake -S . -B build -DBUILD_TESTS=ON -DBUILD_EXAMPLES=ON
(cd build && make -j)
```
