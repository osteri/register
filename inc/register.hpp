#pragma once

#include <cstdint>
#include <limits>
#include <optional>
#include <type_traits>
#include <variant>

#include <../ext/string_id/string_id.hpp>

// Helpers for getting value from std::variant
#define get_val_from(reg) \
  std::get<reg.get_var().index()>(reg.get_var()) & reg.bitmask

namespace Portable {
class RegisterInfo {
 public:
  using name_t = std::variant<const char*, foonathan::string_id::hash_type>;
  using addr_t = std::uint8_t;
  using bitmask_t = std::size_t;

  const name_t name{};
  const addr_t address{};
  const bitmask_t bitmask = std::numeric_limits<std::size_t>::max();
  const float factor = 1.0f;
};

class Register {
 public:
  using var_t = std::variant<std::uint8_t, std::uint16_t>;
  using addr_t = Portable::RegisterInfo::addr_t;
  using bitmask_t = Portable::RegisterInfo::bitmask_t;

  enum class op_t { read, write };
  enum class state_t { init, pending, success, timeout };
  enum class dir_t { rx, tx };

  constexpr explicit Register(RegisterInfo&& info)
      : address{info.address},
        bitmask{info.bitmask},
        m_direction{dir_t::rx},
        m_mask_bits_offset{calc_offset(info.bitmask)} {}

  constexpr Register(RegisterInfo&& info, var_t&& v)
      : address{info.address},
        bitmask{info.bitmask},
        m_var{v},
        m_direction{dir_t::tx},
        m_mask_bits_offset{calc_offset(info.bitmask)} {}

  constexpr var_t get_var() const { return *m_var; }

  constexpr bool has_value() const { return m_var.has_value(); }

  const addr_t address;
  const bitmask_t bitmask;

 protected:
  std::optional<var_t> m_var;
  const op_t m_operation{op_t::read};
  const dir_t m_direction;
  mutable state_t m_state{state_t::init};
  std::uint8_t m_mask_bits_offset;

 private:
  static constexpr std::uint8_t calc_offset(bitmask_t bits) noexcept {
    std::uint8_t ret = 0;
    auto tmp{bits};
    while (tmp) {
      tmp >>= 1;
      ret++;
    }
    return ret;
  }
};
}  // namespace Portable

using RegisterInfo = Portable::RegisterInfo;
using var_t = Portable::Register::var_t;
using name_t = RegisterInfo::name_t;
using addr_t = RegisterInfo::addr_t;
using bitmask_t = RegisterInfo::bitmask_t;