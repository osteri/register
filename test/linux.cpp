#include <gtest/gtest.h>

#include "linux_register.hpp"

using namespace Linux;

TEST(Register, uint8) {
  Register r{{.name = "r", .address = 0x20}, (std::uint8_t)1};
  EXPECT_EQ(1, std::get<std::uint8_t>(r.get_var()) & r.bitmask);
}

TEST(Register, uint8_mask) {
  Register r{{.name = "r", .address = 0x20, .bitmask = 0x1}, (std::uint8_t)1};
  EXPECT_EQ(1, std::get<std::uint8_t>(r.get_var()) & r.bitmask);
}

TEST(Register, uint16_constexpr) {
  constexpr Register r{{.name = "r", .address = 0x20}, (std::uint16_t)0x0FFF};
  constexpr auto v = std::get<std::uint16_t>(r.get_var()) & r.bitmask;
  EXPECT_EQ(0x0FFF, v);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
