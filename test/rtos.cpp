#include <gtest/gtest.h>

#include "rtos_register.hpp"

using namespace foonathan::string_id::literals;

using namespace RTOS;

TEST(Register, uint8) {
  Register r{{.name = "r"_id, .address = 0x20}, (std::uint8_t)1};
  EXPECT_EQ(1, std::get<std::uint8_t>(r.get_var()) & r.bitmask);
}

TEST(Register, uint8_mask) {
  Register r{{.name = "r"_id, .address = 0x20, .bitmask = 0x1}, (std::uint8_t)1};
  EXPECT_EQ(1, std::get<std::uint8_t>(r.get_var()) & r.bitmask);
}

TEST(Register, uint16_constexpr) {
  constexpr Register r{{.name = "r"_id, .address = 0x20}, (std::uint16_t)0x0FFF};
  constexpr auto v = std::get<std::uint16_t>(r.get_var()) & r.bitmask;
  EXPECT_EQ(0x0FFF, v);
}

TEST(Register, hashed_names_are_equal) {
  constexpr Register r1{{.name = "r"_id, .address = 0x20}, (std::uint16_t)0x0FFF};
  constexpr Register r2{{.name = "r"_id, .address = 0x20}, (std::uint16_t)0x0FFF};
  EXPECT_EQ(r1.name, r2.name);
}

int main(int argc, char **argv) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
