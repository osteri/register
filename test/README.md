# This directory contains tests 

Different implementations; for example RTOS and Linux - have different targets.

## Running tests 

```
cmake -S . -B build -DBUILD_TESTS=ON
cd build && make -j
./linux-test
./rtos-test
```
