#include <linux_register.hpp>
#include <yaml-cpp/yaml.h>

// helper type for the visitor (from cppreference)
template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...)
    -> overloaded<Ts...>;  // explicit deduction guide (not needed as of C++20)

namespace Linux {

std::ostream& operator<<(std::ostream& os, const Register& r) {
  /* Print name which can be hash or C string */
  std::visit(overloaded{[&](auto arg) {
               if (std::is_integral_v<decltype(arg)>) {
                 os << "<register hash>: ";
               } else {
                 os << std::get<const char*>(r.name) << ": ";
               }
             }},
             r.name);

  /* Print register value */
  std::visit(
      overloaded{
          [&](auto arg) {
            if (std::is_integral_v<decltype(arg)>) {
              if (std::is_unsigned_v<decltype(arg)>) {
                os << static_cast<unsigned long>(
                    std::get<decltype(arg)>(r.get_var()) & r.bitmask);
              } else {
                os << static_cast<signed long>(
                    std::get<decltype(arg)>(r.get_var()) & r.bitmask);
              }
            } else {
              os << "{}";
            }
          },
      },
      r.get_var());
  return os;
}
}  // namespace Linux