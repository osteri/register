#pragma once

#include <ostream>
#include <register.hpp>
#include <yaml-cpp/yaml.h>

namespace Linux {

class Register : public Portable::Register {
 public:
  constexpr Register(RegisterInfo&& info, var_t&& v)
      : Portable::Register(std::forward<RegisterInfo>(info),
                           std::forward<var_t>(v)),
        name{info.name} {}

  constexpr explicit Register(RegisterInfo&& info)
      : Portable::Register(std::forward<RegisterInfo>(info)), name{info.name} {}

  friend std::ostream& operator<<(std::ostream& os, const Register& r);
  const name_t name;
};
}  // namespace Linux

/* Encode/decode RegisterInfo for yaml node */
namespace YAML {
template <>
struct convert<RegisterInfo> {
  static Node encode(const RegisterInfo& rhs) {
    Node node;
    node["name"] = std::get<const char*>(rhs.name);
    node["address"] = rhs.address;
    node["bitmask"] = rhs.bitmask;
    return node;
  }

  static bool decode(const Node& node, RegisterInfo& rhs) {
    std::string helper;
    try {
      const_cast<name_t&>(rhs.name) = node["name"].as<std::string>().c_str();
      helper = node["address"].as<std::string>();
      const_cast<addr_t&>(rhs.address) = std::stoul(helper, nullptr, 16);
      helper = node["bitmask"].as<std::string>();
      const_cast<bitmask_t&>(rhs.bitmask) = std::stoul(helper, nullptr, 16);
      return true;
    } catch (std::exception& e) {
      return false;
    }
  }
};
}  // namespace YAML