#pragma once

#include <register.hpp>
#include <../ext/string_id/string_id.hpp>

namespace RTOS {

using hash_t = foonathan::string_id::hash_type;

class Register : public Portable::Register {
 public:
  constexpr Register(RegisterInfo&& info, var_t&& v)
      : Portable::Register(std::forward<RegisterInfo>(info),
                           std::forward<var_t>(v)),
        name{std::get<hash_t>(info.name)} {}

  constexpr explicit Register(RegisterInfo&& info)
      : Portable::Register(std::forward<RegisterInfo>(info)),
        name{std::get<hash_t>(info.name)} {}
  const hash_t name;
};
}  // namespace RTOS
