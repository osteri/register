cmake_minimum_required(VERSION 3.13.4)
project(register CXX)

add_subdirectory(inc)

# TODO: only if non-RTOS
# TODO: don't build tests and other stuff
option(YAML_CPP_BUILD_CONTRIB "Enable yaml-cpp contrib in library" OFF)
option(YAML_CPP_BUILD_TOOLS "Enable parse tools" OFF)
option(YAML_CPP_FORMAT_SOURCE "Format source" OFF)
add_subdirectory(ext/yaml-cpp)

# TODO: move compilation of ext/string_id under compilation flag
# TODO: don't build string_id examples
set(FOONATHAN_STRING_ID_DATABASE OFF)
set(FOONATHAN_STRING_ID_MULTITHREADED OFF)
add_subdirectory(ext/string_id)

add_library(linux-register STATIC src/linux/src/linux_register.cpp)
target_include_directories(linux-register PUBLIC src/linux/inc ${CMAKE_CURRENT_BINARY_DIR}/ext/string_id)
add_library(Register::Linux ALIAS linux-register)
target_link_libraries(linux-register register-lib yaml-cpp)

add_library(rtos-register STATIC src/rtos/src/rtos_register.cpp)
target_include_directories(rtos-register PUBLIC src/rtos/inc ${CMAKE_CURRENT_BINARY_DIR}/ext/string_id)
add_library(Register::RTOS ALIAS rtos-register)
target_link_libraries(rtos-register register-lib)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})

# TODO: only for current directory build
if (BUILD_EXAMPLES)
  add_subdirectory(example)
  # copy register_info.yaml
  add_custom_command(
          TARGET linux-register POST_BUILD
          COMMAND ${CMAKE_COMMAND} -E copy
          ${CMAKE_SOURCE_DIR}/example/register_info.yaml
          ${CMAKE_BINARY_DIR}/register_info.yaml)
endif()

if (BUILD_TESTS)
  add_subdirectory(ext/googletest)
  add_subdirectory(test)
endif()
